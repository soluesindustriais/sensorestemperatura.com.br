$(window).on('load', function() {

    //---------- CHECK ASIDE ON PAGE ----------
    
    const checkBread = document.querySelectorAll('#breadcrumb:not([data-bread-sig]) a').length;
    if(checkBread >= 2) {           
        const checkAside = document.querySelector('aside');
        if(checkAside === null) {
            alert("[CHECK HTML]\n\nInsira uma coluna lateral na página!");
        }  
    } 

    //---------- FIND MISSING ATTRIBUTES ----------

    const requiredAttributes = {
        "a": ["href", "title"],
        "img": ["alt", "title"],
        "iframe": ["title"]
    };

    const hideInnerTags = true;
    //TRUE: Alerta contendo apenas a abertura/fechamento da tag.
    //FALSE: Alerta com abertura/fechamento/elementos filhos.

    //Caso não possa corrigir o problema apontado no alerta, adicione o seletor para a tag no :not correspondente
    $(`
        a:not(
            .expand-content,
            .slicknav_menu a,
            .widget__lang a,
            iframe a,
            [class*="elfsight"] a,
            .svg-map a,
            .skiptranslate a
        ), 
        img:not(
            .widget__lang img,
            [src*="fonts.gstatic.com"],
            iframe img,
            [class*="elfsight"] img,
            .skiptranslate img
        ),
        iframe:not(
            [name="votingFrame"],
            [sandbox]
        ) 
    `).each(function() {
        let missingAttributes = "";
        let tagName = $(this).prop("nodeName").toLowerCase();
        const tag = $(this);

        if(typeof requiredAttributes[tagName] !== 'undefined' && typeof requiredAttributes[tagName] !== 'false') {
            requiredAttributes[tagName].forEach(function(tagAttr) {
                if(typeof tag.attr(tagAttr) === 'undefined' || typeof tag.attr(tagAttr) === 'false') {
                    missingAttributes += tagAttr + "\n";
                }
            });
        } else {
            alert("[CHECK HTML]\n\nOs atributos para verificação da tag " + tagName.toUpperCase() + " não foram definidos.");
            return false;
        }
    
        if(missingAttributes != "") {
            const cloneTag = $(this).clone();
            if(hideInnerTags) { cloneTag[0].innerHTML = ""; }  
            alert("[CHECK HTML]\n\nAdicione o(s) seguinte(s) atributo(s) na tag " + tagName.toUpperCase() + ":\n\n" + missingAttributes + "\n" + cloneTag[0].outerHTML);
            return false;
        }
    });
});