<div class="page-container">
    <div class="page-header clearfix">
        <div class="row">
            <?php
            $lv = 3;
            if (!APP_USERS || empty($userlogin) || $user_level < $lv):
                die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "SIGA"));
            endif;
            ?>
            <div class="col-sm-6">
                <h4 class="mt-0 mb-5">Atualização de notas</h4>
                <ol class="breadcrumb mb-0">
                    <li><a href="painel.php">SIGA</a></li>   
                    <li><a href="javascript:;">Notas</a></li>                     
                    <li class="active">Atualizar</li>
                </ol>
            </div>
        </div>
    </div>

    <form id="form-vertical" method="post" novalidate="novalidate" enctype="multipart/form-data">
        <div class="page-content container-fluid">
            <div class="widget">
                <div class="widget-heading clearfix">
                    <h3 class="widget-title pull-left">Atualização de notas</h3>
                    <div class="pull-right">
                        <button type="submit" name="UpdateNotas" class="btn btn-primary"><i class="ti-save"></i></button>
                        <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=notas/index'"><i class="ti-share-alt"></i></button>
                    </div>
                </div>
                <div class="widget-body">
                    <?php
                    $get = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
                    if (isset($post) && isset($post['UpdateNotas'])):
                        $post['notas_id'] = $get;
                        $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
                        unset($post['UpdateNotas']);

                        $update = new AdminNotas();
                        $update->ExeUpdate($post);

                        if (!$update->getResult()):
                            WSErro($update->getError()[0], $update->getError()[1], null, $update->getError()[2]);
                        else:
                            WSErro($update->getError()[0], $update->getError()[1], null, $update->getError()[2]);
                        endif;
                    endif;

                    $Read = new Read;
                    $Read->ExeRead(TB_NOTAS, "WHERE notas_id = :id AND user_empresa = :emp", "id={$get}&emp={$_SESSION['userlogin']['user_empresa']}");
                    if (!$Read->getResult()):
                        WSErro("Não foi possível encontrar o a nota.", WS_ERROR, null, "SIGA");
                    else:
                        extract($Read->getResult()[0]);
                        ?>  
                        <div class="form-group">
                            <label for="Titulo">Titulo</label>
                            <input id="Titulo" type="text" name="notas_titulo" value="<?php
                            if (isset($post['notas_titulo'])): echo $post['notas_titulo'];
                            else:
                                echo $notas_titulo;
                            endif;
                            ?>" placeholder="Digite um título para a nota" data-rule-required="true" data-rule-rangelength="[1,80]" class="form-control">
                        </div> 

                        <div class="form-group">
                            <label for="Mensagem">Mensagem</label>
                            <textarea name="notas_msg" id="Mensagem" placeholder="Digite a mensagem da nota" rows="3" class="form-control"><?php
                                if (isset($post['notas_msg'])): echo $post['notas_msg'];
                                else:
                                    echo $notas_msg;
                                endif;
                                ?></textarea>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </form>
</div>