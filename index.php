<?
$h1         = 'Sensores Temperatura';
$title      = 'Sensores Temperatura | Cotações com diversas empresas especializadas em sensores';
$desc       = 'Faça uma cotação de inumeros tipos de sensores, tudo em um único lugar, aqui na Sensores Temperatura';
$var        = 'Home';
include('inc/head.php');
include('inc/fancy.php');
?>
</head>
<body>

<? include('inc/topo.php'); ?>
<section class="cd-hero">
  <div class="title-main"> <h1><?=$h1?></h1> </div>
  <ul class="cd-hero-slider autoplay">
    <li class="selected">
      <div class="cd-full-width">
        <h2>Sensores Industriais</h2>
        <p class="borda_texto">Sensores são utilizados em aplicações que variam desde o controle de processos até aplicações para segurança operacionais. Logo, diversos detalhes devem ser levados em consideração durante o andamento de características do sensor adequado para cada aplicação.</p>
        <a href="<?=$url?>sensores-industriais" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Sensores de Segurança Industrial</h2>
        <p class="borda_texto">É de grande importância que antes da compra dos sensores de segurança industrial seja decidido qual o tipo de produto será mais eficiente na situação em que será utilizado. Isso porque são comercializados vários modelos de sensores de segurança industrial – tanto magnéticos quanto eletrônicos – que reúnem uma série de características que os diferenciam.</p>
        <a href="<?=$url?>sensores-de-seguranca-industrial" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Sensor de Nível</h2>
        <p  class="borda_texto">Um sensor de nível é um dispositivo utilizado para controlar líquidos ou sólidos granulados acondicionados em reservatórios, silos e tanques – abertos ou pressurizados. O sensor detecta o nível de líquidos em reservatórios, através do movimento dos flutuadores que geram sinais magnéticos (o sinal é transmitido a um sensor magnético).</p>
        <a href="<?=$url?>sensor-de-nivel" class="cd-btn">Saiba mais</a>
      </div>
    </li>
  </ul>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="selected"><a  title="next" href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a  title="next" href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a  title="next" href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
      </ul>
    </nav>
  </div>
</section>
<main>
  <section class="wrapper-main">
    <div class="main-center">
      <div class=" quadro-2 ">
        <h2>Sensor de Temperatura</h2>
        <div class="div-img">
          <p data-anime="left-0">Para a mensuração desta temperatura este sensor de temperatura utiliza um componente cerâmico denominado Termistor que envia sinais elétricos ao relógio medidor no painel do veículo.</p>
        </div>

          <img src="imagens/img-home/automacao-industrial.jpg" alt="Automação industrial" title="Automação industrial">

      </div>
      <div class=" incomplete-box">
        <ul data-anime="in">
          <li>
            <p><b> Saiba mais sobre nossos produtos mais procurados:</b></p>
            
            <li><i class="fas fa-angle-right"></i> <b><a href="<?=$url?>sensor-de-temperatura">Sensor de temperatura</a></b></li>
            <li><i class="fas fa-angle-right"></i> <b><a href="<?=$url?>sensor-infravermelho">Sensor infravermelho</a></b></li>
          <li><i class="fas fa-angle-right"></i> <b><a href="<?=$url?>sensor-ultrassonico">Sensor ultrassônico</a></b></li>
            <li><i class="fas fa-angle-right"></i> <b><a href="<?=$url?>sensor-de-pressao">Sensor de pressão</a></b></li>
            <li><i class="fas fa-angle-right"></i> <b><a href="<?=$url?>termopar-tipo-j">Termopar tipo j</a></b></li>
      
 
          </ul>
          <a href="<?=$url?>sensor-de-temperatura" class="btn-4" data-anime="up">Orçamento Grátis</a>
        </div>
      </div>
      <div id="content-icons">
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <svg version="1.1"
                 id="svg3027" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="154.5px"
                 height="128.75px" viewBox="54.5 66.75 154.5 128.75" enable-background="new 54.5 66.75 154.5 128.75" xml:space="preserve">
              <g id="layer1" transform="translate(0,-796.36218)">
                <g id="g3054" transform="matrix(0.99649078,0,0,0.99674408,0.01493093,2.1552972)">
                  <g id="g3" transform="matrix(8.0150078,0,0,8.022581,13.030797,796.91485)">
                    <path id="path5" fill="#EF843C" d="M12.888,19.041v-7.888c0-0.996-0.808-1.803-1.801-1.803c-0.994,0-1.801,0.805-1.801,1.803
                                                       v7.888c-0.553,0.495-0.9,1.214-0.9,2.013c0,1.491,1.21,2.701,2.701,2.701c1.491,0,2.701-1.21,2.701-2.701
                                                       C13.788,20.255,13.441,19.536,12.888,19.041z M11.087,22.855c-0.994,0-1.801-0.806-1.801-1.801c0-0.666,0.363-1.248,0.901-1.56
                                                       v-8.342c0-0.499,0.403-0.903,0.9-0.903c0.5,0,0.9,0.404,0.9,0.903v8.342c0.538,0.311,0.9,0.893,0.9,1.56
                                                       C12.888,22.049,12.082,22.855,11.087,22.855z"/>
                    <path id="path7" fill="#EF843C" d="M11.53,19.779c0.005-0.025,0.007-0.05,0.007-0.076v-4.499c0-0.249-0.199-0.451-0.45-0.451
                                                       c-0.249,0-0.45,0.205-0.45,0.451v4.499c0,0.026,0.002,0.051,0.006,0.076c-0.528,0.183-0.907,0.685-0.907,1.276
                                                       c0,0.746,0.605,1.351,1.351,1.351c0.746,0,1.351-0.605,1.351-1.351C12.438,20.464,12.059,19.962,11.53,19.779z"/>
                  </g>
                  <g id="g3028" transform="matrix(7.238031,0,0,7.24487,253.65962,772.97402)">
                    <path id="path3" fill="#EF843C" d="M-16.007,20.703c0-0.176,0.072-0.352,0.213-0.48c0.362-0.328,0.57-0.796,0.57-1.283
                                                       c0-0.496-0.201-0.951-0.566-1.282c-0.266-0.241-0.286-0.65-0.045-0.916c0.241-0.266,0.651-0.286,0.916-0.045
                                                       c0.631,0.572,0.993,1.389,0.993,2.243c0,0.853-0.363,1.671-0.995,2.244c-0.266,0.241-0.676,0.221-0.916-0.045
                                                       C-15.952,21.015-16.007,20.859-16.007,20.703L-16.007,20.703z"/>
                    <path id="path5-6" fill="#EF843C" d="M-14.384,22.143c0-0.165,0.063-0.331,0.189-0.457c0.73-0.734,1.133-1.709,1.133-2.745
                                                         c0-1.038-0.401-2.013-1.13-2.747c-0.252-0.254-0.251-0.665,0.003-0.917c0.254-0.253,0.665-0.252,0.917,0.003
                                                         c0.972,0.977,1.508,2.278,1.508,3.661c0,1.381-0.536,2.681-1.51,3.66c-0.253,0.254-0.663,0.255-0.918,0.002
                                                         C-14.32,22.476-14.384,22.309-14.384,22.143L-14.384,22.143z"/>
                    <path id="path7-5" fill="#EF843C" d="M-12.764,23.579c0-0.161,0.06-0.323,0.18-0.449c1.086-1.132,1.683-2.62,1.683-4.19
                                                         c0-1.573-0.597-3.061-1.681-4.191c-0.248-0.258-0.24-0.669,0.019-0.917c0.259-0.248,0.669-0.24,0.917,0.018
                                                         c1.317,1.373,2.043,3.18,2.043,5.089c0,1.906-0.726,3.712-2.044,5.088c-0.248,0.259-0.659,0.267-0.917,0.019
                                                         C-12.697,23.92-12.764,23.749-12.764,23.579z"/>
                  </g>
                </g>
              </g>
            </svg>
            <div>
              <p>Grande variedade de sensores</p>
            </div>
          </div>
        </div>
 
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
           <img src="<?=$url?>imagens/img-home/tachometer-icon.svg" alt="Sensores de Temperatura" />
            <div>
              <p>Cotações de trasmissores</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-user-clock fa-7x"></i>
            <div>
              <p>Agilidade nas entregas</p>
            </div>
          </div>
        </div> 
      </div>
    </section>
    <section class="wrapper-img">
      <div class="txtcenter">
        <h2>Produtos <b>Relacionados</b></h2>
      </div>
      <div class="content-icons">
        <div class="produtos-relacionados-1">
          <figure>
            <a href="<?=$url?>sensor-infravermelho">
              <div class="fig-img">
                <h2>Transmissor de <br>	 temperatura</h2>
                <p>Transmissores de temperatura convertem os sinais de vários tipos de sensores como termorresistência, termopares e também potenciômetros, em um indício de saída padronizado.</p>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-2">
          <figure class="figure2">
            <a href="<?=$url?>sensor-de-pressao">
              <div class="fig-img2">
                <h2>Sensor de pressão</h2>
                <p>O sensor de pressão converte uma quantidade física ‘pressão’ em um sinal de padrão industrial.</p>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-3">
          <figure>
            <a href="<?=$url?>transmissor-de-temperatura-com-indicacao">
              <div class="fig-img">
                <h2>Transmissor de <br>	 temperatura com indicação</h2>
                <p>Os Transmissores de temperatura com indicação convertem um sinal emitido pelo sensor de temperatura, em sinal diretamente proporcional em corrente.</p>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
      </div>
    </section>
    <section class="wrapper-destaque">
      <div class="destaque txtcenter">
        <h2>Galeria de <b>Produtos</b></h2>
        <div class="center-block txtcenter">
          <ul class="gallery">
            <li><a href="<?=$url?>imagens/img-home/foto-01.jpg" class="lightbox" title="Sensor de Temperatura Digital">
              <img src="<?=$url?>imagens/img-home/thumbs/foto-01.jpg" title="Sensor de Temperatura Digital" alt="Sensor de Temperatura Digital">
            </a>
          </li>
          <li><a href="<?=$url?>imagens/img-home/foto-02.jpg" class="lightbox"  title="foto-02.jpg">
            <img src="<?=$url?>imagens/img-home/thumbs/foto-02.jpg" alt="foto-02.jpg" title="foto-02.jpg">
          </a>
        </li>
            <li><a href="<?=$url?>imagens/img-home/foto-03.jpg" class="lightbox" title="foto-03.jpg">
          <img src="<?=$url?>imagens/img-home/thumbs/foto-03.jpg" alt="foto-03.jpg" title="foto-03.jpg">
        </a>
      </li>
      <li><a href="<?=$url?>imagens/img-home/foto-04.jpg" class="lightbox" title="foto-04.jpg">
        <img src="<?=$url?>imagens/img-home/thumbs/foto-04.jpg" alt="foto-04.jpg" title="foto-04.jpg">
      </a>
    </li>
    <li><a href="<?=$url?>imagens/img-home/foto-05.jpg" class="lightbox" title="foto-05.jpg">
      <img src="<?=$url?>imagens/img-home/thumbs/foto-05.jpg" alt="foto-05.jpg"  title="foto-05.jpg">
    </a>
  </li>
  <li><a href="<?=$url?>imagens/img-home/foto-06.jpg" class="lightbox" title="foto-06.jpg">
    <img src="<?=$url?>imagens/img-home/thumbs/foto-06.jpg" alt="foto-06.jpg" title="foto-06.jpg">
  </a>
</li>
<li><a href="<?=$url?>imagens/img-home/foto-07.jpg" class="lightbox" title="foto-07.jpg">
  <img src="<?=$url?>imagens/img-home/thumbs/foto-07.jpg" alt="foto-07.jpg" title="foto-07.jpg">
</a>
</li>
<li><a href="<?=$url?>imagens/img-home/foto-08.jpg" class="lightbox" title="foto-08.jpg">
<img src="<?=$url?>imagens/img-home/thumbs/foto-08.jpg" alt="foto-08.jpg" title="foto-08.jpg">
</a>
</li>
<li><a href="<?=$url?>imagens/img-home/foto-09.jpg" class="lightbox" title="foto-09.jpg">
<img src="<?=$url?>imagens/img-home/thumbs/foto-09.jpg" alt="foto-09.jpg" title="foto-09.jpg">
</a>
</li>
<li><a href="<?=$url?>imagens/img-home/foto-10.jpg" class="lightbox" title="foto-10.jpg">
<img src="<?=$url?>imagens/img-home/thumbs/foto-10.jpg" alt="foto-10.jpg" title="foto-10.jpg">
</a>
</li>
</ul>
</div>
</div>
</section>
</main>
<? include('inc/footer.php'); ?>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>
</body>
</html>