<footer>
<?php include 'inc/fancy.php'; ?>
	<div class="wrapper">
		<div class="contact-footer">
			<address>
				<span><?=$nomeSite." - ".$slogan?></span>
			</address>
		</div>
		<div class="menu-footer">
			<nav>
				<ul>
					<li><a rel="nofollow" href="<?=$url?>" title="Página inicial">Home</a></li>
					<li><a rel="nofollow" href="<?=$url?>informacoes" title="Informacoes">Informações</a></li>
					<li><a href="<?=$url?>sobre-nos" title="Sobre nós">Sobre nós</a></li>
					<li><a rel="nofollow" href="<?=$url?>sensores" title="Sensores">Sensores</a></li>
					<li><a href="<?=$url?>mapa-site" title="Mapa do site <?=$nomeSite?>">Mapa do site</a></li>
				</ul>
			</nav>
		</div>
		<br class="clear">
	</div>
</footer>
<div class="copyright-footer">
	<div class="wrapper-footer">
		Copyright © <?=$nomeSite?>. (Lei 9610 de 19/02/1998)
		<div class="center-footer">
			<img src="imagens/img-home/logo-footer.png" alt="<?=$nomeSite?>" title="<?=$nomeSite?>">
			<p>é um parceiro</p>
			<img src="imagens/logo-solucs.png" alt="Soluções Industriais" title="Soluções Industriais">
		</div>
		<div class="selos">
			<a rel="noopener nofollow" href="http://validator.w3.org/check?uri=<?=$url.$urlPagina?>" target="_blank" title="HTML5 W3C"><i class="fab fa-html5"></i> <strong>W3C</strong></a>
			<a rel="noopener nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?=$url.$urlPagina?>" target="_blank" title="CSS W3C" ><i class="fab fa-css3"></i> <strong>W3C</strong></a>
		</div>
	</div>
</div>
<script defer src="<?=$url?>js/geral.js"></script>
<script>
var guardar = document.querySelectorAll('.botao-cotar');
for(var i = 0; i < guardar.length; i++){
  var adicionando = guardar[i].parentNode;
  adicionando.classList.add('nova-api');
};
</script>

<!-- START API -->
<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>
<!-- END API -->
<!-- Google Analytics -->
<script>
	(function(i,s,o,g,r,a,m){
	i['GoogleAnalyticsObject']=r;
	i[r]=i[r] || function(){(
	i[r].q=i[r].q||[]).push(arguments)},
	i[r].l=1*new Date();
	a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];
	a.async=1;
	a.src=g;
	m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-121399258-1', 'auto');
	ga('create', 'UA-47730935-51', 'auto', 'clientTracker');
	ga('send', 'pageview');
	ga('clientTracker.send', 'pageview');
</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-57L8NW1EW6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-57L8NW1EW6');
</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-D23WW3S4NC');
</script>




<!-- BOTAO SCROLL -->
<script async src="<?=$url?>js/jquery.scrollUp.min.js"></script>
<script async src="<?=$url?>js/scroll.js"></script>
<!-- /BOTAO SCROLL -->
<script async src="<?=$url?>js/vendor/modernizr-2.6.2.min.js"></script>
<script async src="<?=$url?>js/app.js"></script>
<!--<script src="js/click-actions.js"></script> -->

<!-- Script Launch start -->
<script src="https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js" defer></script>
<script> const aside = document.querySelector('aside');const data = '<div data-sdk-ideallaunch data-segment="Soluções Industriais - Oficial"></div>'; aside != null ? aside.insertAdjacentHTML('afterbegin', data) : console.log("Não há aside presente para o Launch");  
</script> <!-- Script Launch end -->