
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('Empresa de sensor fotoelétrico','Fornecedor de sensor fotoelétrico','Fornecedor de sensor indutivo','Sensor capacitivo analogico','Sensor capacitivo e indutivo','Sensor de encoder absoluto','Sensor de Fluxo','Sensor de fluxo de água','Sensor de posição encoder','Sensor encoder','Sensor encoder incremental','Sensor fotoelétrico alta temperatura','Sensor fotoelétrico industrial','Sensor indutivo','Sensor indutivo 2 fios','Sensor indutivo 4 fios','Sensor indutivo analógico','Sensor indutivo e capacitivo','Sensor indutivo para alta temperatura','Sensor magnético','Sensor magnético industrial'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "sensores";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/sensores/sensores-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-20"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/sensores/sensores-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/sensores/thumbs/sensores-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>