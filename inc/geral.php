<?
$nomeSite			= 'Sensores de Temperatura';
$slogan				= 'Sensores de diversos tipos | Cotações gratuitas'; 
$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/"; }
else { $url = $http."://".$host.$dir["dirname"]."/";  }
$emailContato		= 'everton.lima@solucoesindustriais.com.br';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idAnalytics		= 'UA-121399258-1';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
//Breadcrumbs
$caminho  = '<div class="breadcrumb" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="4">
            </li>
        </ol>
    </nav>
</div>    
</div>';
$caminho2     = '<div class="breadcrumb" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'produtos" itemprop="item" title="Produtos">
                <span itemprop="name"> Produtos</span>
            </a>
            <meta itemprop="position" content="2">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="4">
            </li>
        </ol>
    </nav>
</div>    
</div>';

$caminhoinformacoes='<div class="breadcrumb" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'informacoes" itemprop="item" title="Informações">
                <span itemprop="name"> Informações </span>
            </a>
            <meta itemprop="position" content="2">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="4">
            </li>
        </ol>
    </nav>
</div>    
</div>';

$caminhosensores='<div class="breadcrumb" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'sensores" itemprop="item" title="Sensores">
                <span itemprop="name"> Sensores </span>
            </a>
            <meta itemprop="position" content="2">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="4">
            </li>
        </ol>
    </nav>
</div>    
</div>';


?>